<script runat="server" language="javascript">
    Platform.Load("Core", "1");
    HTTPHeader.Remove("Cache-Control");
    HTTPHeader.SetValue("Cache-Control", "no-cache,no-store");

    var response = {
        "response": "Invalid token"
    };

    var credentials = DataExtension.Init("credentials");
    var data = credentials.Rows.Lookup(["platform"], ["atenea"]);
    var jsonpost = Platform.Function.ParseJSON(Platform.Request.GetPostData());

    if (jsonpost.token == data[0]["token"]) {
        var subsDE = DataExtension.Init("_Subscribers");
        var subsRes = subsDE.Rows.Lookup(['SubscriberKey'], [jsonpost.data.subscriberKey]);

        if (subsRes.length == 1) {
            var contactID = subsRes[0]['SubscriberID'];

            var email = subsRes[0]['EmailAddress'];

            var mobileConnectDE = DataExtension.Init("_MobileAddress");
            var mobileRes = mobileConnectDE.Rows.Lookup(
                ['_ContactID', '_Priority'], [contactID, 1]
            );
            var mobile = mobileRes[0]['_MobileNumber'];

            response = {
                "contactID": contactID,
                "email": email,
                "mobile": mobile
            };
        } else {
            response = "Customer does not exists";
        }
    }
    Write(Stringify(response));
</script>