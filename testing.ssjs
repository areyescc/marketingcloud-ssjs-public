<script runat="server" language="javascript">
    Platform.Load("Core", "1");

    var response = [];
    var credentials = DataExtension.Init("credentials");
    var data = credentials.Rows.Lookup(["platform"], ["atenea"]);
    var jsonpost = Platform.Function.ParseJSON(Platform.Request.GetPostData());

    if (jsonpost.token == data[0]["token"]) {
        var subsDE = DataExtension.Init("_Subscribers");
        var subsRes = subsDE.Rows.Lookup(['SubscriberKey'], [jsonpost.data.subscriberKey]);

        var contactID = subsRes[0]['SubscriberID'];

        if (subsRes.length == 1) {
            if (typeof jsonpost.data.phone == 'string') {
                var mobileConnectDE = DataExtension.Init("_MobileAddress");

                var subMobile = mobileConnectDE.Rows.Lookup(['_ContactID'], [contactID]);

                if (subMobile == null) {
                    try {
                        var data = {
                            _MobileNumber: jsonpost.data.phone,
                            _ContactID: contactID,
                            _CountryCode: "ES"
                        }

                        mobileConnectDE.Rows.Add(data);

                        response.push("Phone inserted");
                    } catch (error) {
                        response.push("Error inserting phone");
                    }
                } else {
                    try {
                        var data = {
                            _MobileNumber: jsonpost.data.phone
                        }

                        mobileConnectDE.Rows.Update({
                            data
                        }, ["_ContactID", "_Priority"], [contactID, 1]);

                        response.push("Phone updated");
                    } catch (error) {
                        response.push("Error updating phone");
                    }
                }
            }
            if (typeof jsonpost.data.email == 'string') {
                try {
                    Subscriber.Init(subsRes[0]['SubscriberKey'])
                        .Update({
                            "EmailAddress": jsonpost.data.email
                        });
                    response.push("Email updated");
                } catch (error) {
                    response.push("Error updating email");
                }
            }
        } else {
            response = ["Customer does not exists"];
        }
    }
    Write(Stringify(response));
</script>